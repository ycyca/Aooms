package net.aooms.core.module.mybatis.record;

/**
 * Record操作
 * Created by 风象南(cheereebo) on 2018/9/7
 */
public interface IRecordOper {

    void process();

}
